/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 10.4.19-MariaDB : Database - instansikesehatan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`instansikesehatan` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `instansikesehatan`;

/*Table structure for table `master_jenislayanan` */

DROP TABLE IF EXISTS `master_jenislayanan`;

CREATE TABLE `master_jenislayanan` (
  `id_jenis_layanan` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_registrasi` int(11) DEFAULT NULL,
  `nama_layanan` varchar(32) DEFAULT NULL,
  `is_active` int(1) DEFAULT 1,
  PRIMARY KEY (`id_jenis_layanan`),
  KEY `master_jenislayanan_ibfk_1` (`id_jenis_registrasi`),
  CONSTRAINT `master_jenislayanan_ibfk_1` FOREIGN KEY (`id_jenis_registrasi`) REFERENCES `master_jenisregistrasi` (`id_jenis_registrasi`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_jenislayanan` */

insert  into `master_jenislayanan`(`id_jenis_layanan`,`id_jenis_registrasi`,`nama_layanan`,`is_active`) values 
(1,1,'Polikliniik Umum',1),
(2,1,'Poliklinik Gigi',1),
(3,1,'Poliklinik Obgyn',1),
(4,1,'Poliklinik Mata',1),
(5,2,'Kelas 1',1),
(6,2,'Kelas 2',1),
(7,2,'Kelas 3',1),
(8,3,'IGD',1);

/*Table structure for table `master_jenispembayaran` */

DROP TABLE IF EXISTS `master_jenispembayaran`;

CREATE TABLE `master_jenispembayaran` (
  `id_jenis_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pembayaran` varchar(32) DEFAULT NULL,
  `is_active` int(1) DEFAULT 1,
  PRIMARY KEY (`id_jenis_pembayaran`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_jenispembayaran` */

insert  into `master_jenispembayaran`(`id_jenis_pembayaran`,`nama_pembayaran`,`is_active`) values 
(1,'Umum',1),
(2,'BPJS Kesehatan',1),
(3,'Mandiri Inhealth',1),
(4,'BNI Life',1);

/*Table structure for table `master_jenisregistrasi` */

DROP TABLE IF EXISTS `master_jenisregistrasi`;

CREATE TABLE `master_jenisregistrasi` (
  `id_jenis_registrasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_registrasi` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_registrasi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_jenisregistrasi` */

insert  into `master_jenisregistrasi`(`id_jenis_registrasi`,`nama_registrasi`) values 
(1,'Rawat Jalan'),
(2,'Rawat Inap'),
(3,'IGD');

/*Table structure for table `master_jk` */

DROP TABLE IF EXISTS `master_jk`;

CREATE TABLE `master_jk` (
  `id_jk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jk` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id_jk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_jk` */

insert  into `master_jk`(`id_jk`,`nama_jk`) values 
(1,'Laki - laki'),
(2,'Perempuan');

/*Table structure for table `master_statusregistrasi` */

DROP TABLE IF EXISTS `master_statusregistrasi`;

CREATE TABLE `master_statusregistrasi` (
  `id_status_registrasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_registrasi` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_status_registrasi`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_statusregistrasi` */

insert  into `master_statusregistrasi`(`id_status_registrasi`,`nama_registrasi`) values 
(1,'Aktif'),
(2,'Tutup Kuunjungan');

/*Table structure for table `master_userrole` */

DROP TABLE IF EXISTS `master_userrole`;

CREATE TABLE `master_userrole` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `master_userrole` */

insert  into `master_userrole`(`role_id`,`nama_role`) values 
(1,'Admin'),
(2,'User');

/*Table structure for table `trx_pasien` */

DROP TABLE IF EXISTS `trx_pasien`;

CREATE TABLE `trx_pasien` (
  `norm` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(64) DEFAULT NULL,
  `tempat_lahir` varchar(64) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` int(1) DEFAULT NULL,
  `is_active` int(1) DEFAULT 1,
  `waktu_input` datetime DEFAULT NULL,
  `waktu_update` datetime DEFAULT NULL,
  PRIMARY KEY (`norm`),
  KEY `jenis_kelamin` (`jenis_kelamin`),
  CONSTRAINT `trx_pasien_ibfk_1` FOREIGN KEY (`jenis_kelamin`) REFERENCES `master_jk` (`id_jk`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `trx_pasien` */

insert  into `trx_pasien`(`norm`,`nama`,`tempat_lahir`,`tanggal_lahir`,`jenis_kelamin`,`is_active`,`waktu_input`,`waktu_update`) values 
(1,'Tina Martha','Pontianak','1990-02-03',2,1,'2022-11-14 20:20:26','2022-11-14 20:20:26'),
(2,'Radinal Dwiki Novendra','Pekan Baru','1995-11-10',1,1,'2022-11-14 20:22:09','2022-11-14 20:22:09'),
(3,'Desy Arjuna','Jakarta','1993-12-06',2,1,'2022-11-14 20:23:20','2022-11-14 20:23:20'),
(4,'Alfi Syahri','Bandung','1995-01-01',1,1,'2022-11-14 20:24:35','2022-11-14 20:24:35'),
(5,'Nanang Sunardi','Bandung','1989-02-04',1,1,'2022-11-14 20:25:27','2022-11-14 20:25:27'),
(6,'Suzana','Pontianak','1990-01-02',2,1,'2022-11-14 20:28:41','2022-11-14 20:28:51'),
(7,'Rikko','Ella Hilir','1996-03-04',1,1,'2022-11-14 20:30:28','2022-11-14 20:30:28');

/*Table structure for table `trx_pendaftaran` */

DROP TABLE IF EXISTS `trx_pendaftaran`;

CREATE TABLE `trx_pendaftaran` (
  `id_pendaftaran` int(11) NOT NULL AUTO_INCREMENT,
  `no_registrasi` int(10) DEFAULT NULL,
  `waktu_registrasi` datetime DEFAULT NULL,
  `norm` int(11) DEFAULT NULL,
  `id_jenis_registrasi` int(11) DEFAULT NULL,
  `id_jenis_layanan` int(11) DEFAULT NULL,
  `id_jenis_pembayaran` int(11) DEFAULT NULL,
  `id_status_registrasi` int(11) DEFAULT NULL,
  `waktu_selesai_pelayanan` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pendaftaran`),
  KEY `trx_pendaftaran_ibfk_2` (`id_jenis_registrasi`),
  KEY `trx_pendaftaran_ibfk_3` (`id_jenis_layanan`),
  KEY `trx_pendaftaran_ibfk_4` (`id_jenis_pembayaran`),
  KEY `trx_pendaftaran_ibfk_6` (`id_status_registrasi`),
  KEY `trx_pendaftaran_ibfk_7` (`id_user`),
  KEY `trx_pendaftaran_ibfk_1` (`norm`),
  CONSTRAINT `trx_pendaftaran_ibfk_1` FOREIGN KEY (`norm`) REFERENCES `trx_pasien` (`norm`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trx_pendaftaran_ibfk_2` FOREIGN KEY (`id_jenis_registrasi`) REFERENCES `master_jenisregistrasi` (`id_jenis_registrasi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trx_pendaftaran_ibfk_3` FOREIGN KEY (`id_jenis_layanan`) REFERENCES `master_jenislayanan` (`id_jenis_layanan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trx_pendaftaran_ibfk_4` FOREIGN KEY (`id_jenis_pembayaran`) REFERENCES `master_jenispembayaran` (`id_jenis_pembayaran`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trx_pendaftaran_ibfk_5` FOREIGN KEY (`id_status_registrasi`) REFERENCES `master_statusregistrasi` (`id_status_registrasi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trx_pendaftaran_ibfk_7` FOREIGN KEY (`id_user`) REFERENCES `trx_user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

/*Data for the table `trx_pendaftaran` */

insert  into `trx_pendaftaran`(`id_pendaftaran`,`no_registrasi`,`waktu_registrasi`,`norm`,`id_jenis_registrasi`,`id_jenis_layanan`,`id_jenis_pembayaran`,`id_status_registrasi`,`waktu_selesai_pelayanan`,`id_user`) values 
(7,3,'2022-11-15 02:08:34',1,1,1,1,1,NULL,1),
(8,12,'2022-11-15 02:21:45',1,1,1,1,1,NULL,1),
(9,2147483647,'2022-11-15 02:44:26',5,1,1,1,1,NULL,NULL),
(10,2147483647,'2022-11-15 02:45:09',6,1,1,1,1,NULL,NULL),
(11,2147483647,'2022-11-15 02:46:57',3,1,1,1,1,NULL,NULL),
(12,2147483647,'2022-11-15 02:48:20',1,1,1,1,1,NULL,NULL),
(13,2147483647,'2022-11-15 02:49:05',6,1,1,1,1,NULL,NULL),
(14,2147483647,'2022-11-15 02:52:02',6,1,1,1,1,NULL,NULL);

/*Table structure for table `trx_user` */

DROP TABLE IF EXISTS `trx_user`;

CREATE TABLE `trx_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(64) NOT NULL,
  `user_name` varchar(64) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `role_id` int(2) DEFAULT NULL,
  `is_active` int(1) DEFAULT 1,
  `waktu_input` datetime DEFAULT NULL,
  `waktu_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `trx_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `master_userrole` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `trx_user` */

insert  into `trx_user`(`id_user`,`nama`,`user_name`,`password`,`role_id`,`is_active`,`waktu_input`,`waktu_update`) values 
(1,'Admin Sistem','admin','admin',1,1,'2022-11-13 12:19:05','2022-11-13 12:19:05'),
(2,'Ahmad Sandi','ahmad','ahmad',2,1,'2022-11-13 12:19:44','2022-11-13 12:19:44'),
(3,'Cici Utami','cici','cici',2,1,'2022-11-13 12:20:15','2022-11-13 12:20:15'),
(4,'Putri Aisha','putri','putri',2,1,'2022-11-13 12:25:02','2022-11-13 12:25:02'),
(5,'Safitri Jayanti','safitri','safitri',2,1,'2022-11-13 12:25:54','2022-11-13 12:25:54');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
