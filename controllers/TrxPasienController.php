<?php

namespace app\controllers;

use app\models\TrxPasien;
use app\models\TrxPasienSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrxPasienController implements the CRUD actions for TrxPasien model.
 */
class TrxPasienController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TrxPasien models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TrxPasienSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrxPasien model.
     * @param int $norm Norm
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($norm)
    {
        return $this->render('view', [
            'model' => $this->findModel($norm),
        ]);
    }

    /**
     * Creates a new TrxPasien model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new TrxPasien();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'norm' => $model->norm]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TrxPasien model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $norm Norm
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($norm)
    {
        $model = $this->findModel($norm);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'norm' => $model->norm]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TrxPasien model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $norm Norm
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($norm)
    {
        $this->findModel($norm)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxPasien model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $norm Norm
     * @return TrxPasien the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($norm)
    {
        if (($model = TrxPasien::findOne(['norm' => $norm])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
