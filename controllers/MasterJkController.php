<?php

namespace app\controllers;

use app\models\MasterJk;
use app\models\MasterJkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterJkController implements the CRUD actions for MasterJk model.
 */
class MasterJkController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MasterJk models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new MasterJkSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterJk model.
     * @param int $id_jk Id Jk
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_jk)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_jk),
        ]);
    }

    /**
     * Creates a new MasterJk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new MasterJk();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_jk' => $model->id_jk]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterJk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_jk Id Jk
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_jk)
    {
        $model = $this->findModel($id_jk);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_jk' => $model->id_jk]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MasterJk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_jk Id Jk
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_jk)
    {
        $this->findModel($id_jk)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterJk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_jk Id Jk
     * @return MasterJk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_jk)
    {
        if (($model = MasterJk::findOne(['id_jk' => $id_jk])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
