<?php

namespace app\controllers;

use app\models\TrxPendaftaran;
use app\models\TrxPendaftaranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrxPendaftaranController implements the CRUD actions for TrxPendaftaran model.
 */
class TrxPendaftaranController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TrxPendaftaran models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TrxPendaftaranSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrxPendaftaran model.
     * @param int $id_pendaftaran Id Pendaftaran
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_pendaftaran)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_pendaftaran),
        ]);
    }

    /**
     * Creates a new TrxPendaftaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new TrxPendaftaran();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_pendaftaran' => $model->id_pendaftaran]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TrxPendaftaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_pendaftaran Id Pendaftaran
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_pendaftaran)
    {
        $model = $this->findModel($id_pendaftaran);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_pendaftaran' => $model->id_pendaftaran]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TrxPendaftaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_pendaftaran Id Pendaftaran
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_pendaftaran)
    {
        $this->findModel($id_pendaftaran)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxPendaftaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_pendaftaran Id Pendaftaran
     * @return TrxPendaftaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_pendaftaran)
    {
        if (($model = TrxPendaftaran::findOne(['id_pendaftaran' => $id_pendaftaran])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
