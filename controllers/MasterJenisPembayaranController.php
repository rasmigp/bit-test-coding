<?php

namespace app\controllers;

use app\models\MasterJenispembayaran;
use app\models\MasterJenispembayaranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterJenisPembayaranController implements the CRUD actions for MasterJenispembayaran model.
 */
class MasterJenisPembayaranController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MasterJenispembayaran models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new MasterJenispembayaranSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterJenispembayaran model.
     * @param int $id_jenis_pembayaran Id Jenis Pembayaran
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_jenis_pembayaran)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_jenis_pembayaran),
        ]);
    }

    /**
     * Creates a new MasterJenispembayaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new MasterJenispembayaran();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_jenis_pembayaran' => $model->id_jenis_pembayaran]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterJenispembayaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_jenis_pembayaran Id Jenis Pembayaran
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_jenis_pembayaran)
    {
        $model = $this->findModel($id_jenis_pembayaran);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_jenis_pembayaran' => $model->id_jenis_pembayaran]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MasterJenispembayaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_jenis_pembayaran Id Jenis Pembayaran
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_jenis_pembayaran)
    {
        $this->findModel($id_jenis_pembayaran)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterJenispembayaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_jenis_pembayaran Id Jenis Pembayaran
     * @return MasterJenispembayaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_jenis_pembayaran)
    {
        if (($model = MasterJenispembayaran::findOne(['id_jenis_pembayaran' => $id_jenis_pembayaran])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
