<?php

namespace app\controllers;

use app\models\MasterStatusregistrasi;
use app\models\MasterStatusregistrasiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterStatusregistrasiController implements the CRUD actions for MasterStatusregistrasi model.
 */
class MasterStatusregistrasiController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MasterStatusregistrasi models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new MasterStatusregistrasiSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterStatusregistrasi model.
     * @param int $id_status_registrasi Id Status Registrasi
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_status_registrasi)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_status_registrasi),
        ]);
    }

    /**
     * Creates a new MasterStatusregistrasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new MasterStatusregistrasi();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_status_registrasi' => $model->id_status_registrasi]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterStatusregistrasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_status_registrasi Id Status Registrasi
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_status_registrasi)
    {
        $model = $this->findModel($id_status_registrasi);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_status_registrasi' => $model->id_status_registrasi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MasterStatusregistrasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_status_registrasi Id Status Registrasi
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_status_registrasi)
    {
        $this->findModel($id_status_registrasi)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterStatusregistrasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_status_registrasi Id Status Registrasi
     * @return MasterStatusregistrasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_status_registrasi)
    {
        if (($model = MasterStatusregistrasi::findOne(['id_status_registrasi' => $id_status_registrasi])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
