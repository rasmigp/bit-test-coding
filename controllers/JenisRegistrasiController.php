<?php

namespace app\controllers;

use app\models\MasterJenisRegistrasi;
use app\models\JenisRegistrasiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JenisRegistrasiController implements the CRUD actions for MasterJenisRegistrasi model.
 */
class JenisRegistrasiController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MasterJenisRegistrasi models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new JenisRegistrasiSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterJenisRegistrasi model.
     * @param int $id_jenis_registrasi Id Jenis Registrasi
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_jenis_registrasi)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_jenis_registrasi),
        ]);
    }

    /**
     * Creates a new MasterJenisRegistrasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new MasterJenisRegistrasi();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_jenis_registrasi' => $model->id_jenis_registrasi]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterJenisRegistrasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_jenis_registrasi Id Jenis Registrasi
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_jenis_registrasi)
    {
        $model = $this->findModel($id_jenis_registrasi);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_jenis_registrasi' => $model->id_jenis_registrasi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MasterJenisRegistrasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_jenis_registrasi Id Jenis Registrasi
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_jenis_registrasi)
    {
        $this->findModel($id_jenis_registrasi)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterJenisRegistrasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_jenis_registrasi Id Jenis Registrasi
     * @return MasterJenisRegistrasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_jenis_registrasi)
    {
        if (($model = MasterJenisRegistrasi::findOne(['id_jenis_registrasi' => $id_jenis_registrasi])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
