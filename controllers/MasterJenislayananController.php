<?php

namespace app\controllers;

use app\models\MasterJenislayanan;
use app\models\MasterJenislayananSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterJenislayananController implements the CRUD actions for MasterJenislayanan model.
 */
class MasterJenislayananController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MasterJenislayanan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new MasterJenislayananSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterJenislayanan model.
     * @param int $id_jenis_layanan Id Jenis Layanan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_jenis_layanan)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_jenis_layanan),
        ]);
    }

    /**
     * Creates a new MasterJenislayanan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new MasterJenislayanan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_jenis_layanan' => $model->id_jenis_layanan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterJenislayanan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_jenis_layanan Id Jenis Layanan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_jenis_layanan)
    {
        $model = $this->findModel($id_jenis_layanan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_jenis_layanan' => $model->id_jenis_layanan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MasterJenislayanan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_jenis_layanan Id Jenis Layanan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_jenis_layanan)
    {
        $this->findModel($id_jenis_layanan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterJenislayanan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_jenis_layanan Id Jenis Layanan
     * @return MasterJenislayanan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_jenis_layanan)
    {
        if (($model = MasterJenislayanan::findOne(['id_jenis_layanan' => $id_jenis_layanan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
