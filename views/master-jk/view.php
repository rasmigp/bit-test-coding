<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\MasterJk $model */

$this->title = $model->id_jk;
$this->params['breadcrumbs'][] = ['label' => 'Master Jks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="master-jk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_jk' => $model->id_jk], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_jk' => $model->id_jk], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_jk',
            'nama_jk',
        ],
    ]) ?>

</div>
