<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterJk $model */

$this->title = 'Update Master Jk: ' . $model->id_jk;
$this->params['breadcrumbs'][] = ['label' => 'Master Jks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_jk, 'url' => ['view', 'id_jk' => $model->id_jk]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-jk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
