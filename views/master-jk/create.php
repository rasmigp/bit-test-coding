<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterJk $model */

$this->title = 'Create Master Jk';
$this->params['breadcrumbs'][] = ['label' => 'Master Jks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-jk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
