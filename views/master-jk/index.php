<?php

use app\models\MasterJk;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\MasterJkSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Master Jks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-jk-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Master Jk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_jk',
            'nama_jk',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, MasterJk $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_jk' => $model->id_jk]);
                 }
            ],
        ],
    ]); ?>


</div>
