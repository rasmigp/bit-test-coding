<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

// use kartikorm\ActiveForm;
use kartik\date\DatePicker;

/** @var yii\web\View $this */
/** @var app\models\TrxPasien $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="trx-pasien-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

    <?php 
        // echo '<label class="form-label">Tanggal Lahir</label>';
        echo $form->field($model, 'tanggal_lahir')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Enter birth date ...'],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
    ?>

    <?php
    $dataPost=ArrayHelper::map(\app\models\MasterJk::find()->asArray()->all(), 'id_jk', 'nama_jk');
	echo $form->field($model, 'jenis_kelamin')
        ->dropDownList(
            $dataPost,           
            ['id_jk'=>'nama_jk']
        );
        ?>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
