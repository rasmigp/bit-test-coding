<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TrxPasien $model */

$this->title = 'Input Pasien Baru';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Pasien', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-pasien-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
