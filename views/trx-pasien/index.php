<?php

use app\models\TrxPasien;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\TrxPasienSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Daftar Pasien';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-pasien-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Input Data Pasien', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'norm',
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'jenisKelamin.nama_jk',
            'is_active',
            //'waktu_input',
            //'waktu_update',
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {update} {delete} {daftar}',

                'urlCreator' => function ($action, TrxPasien $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'norm' => $model->norm]);
                 },

                //  'class' => 'yii\grid\ActionColumn',

                
                'buttons'=> [   
                  'daftar' => function ($url, $model) {

                    $url= 'index.php?r=trx-pendaftaran%2Fcreate&norm='.$model->norm;
                     return Html::a('daftar', $url, [
                        'title' => "Daftar",
                        'class' => 'btn btn-xs btn-success',
                        'data' => [
                            'method' => 'post',
                            // 'confirm' => 'Are you sure? This will Suspend this.',
                        ],
                     ]);
                   }
                ],
            ],
        ],
    ]); ?>


</div>
