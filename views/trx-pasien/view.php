<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\TrxPasien $model */

$this->title = $model->norm;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Pasien', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="trx-pasien-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'norm' => $model->norm], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'norm' => $model->norm], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'norm',
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'jenis_kelamin',
            'is_active',
            'waktu_input',
            'waktu_update',
        ],
    ]) ?>

</div>
