<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TrxPasien $model */

$this->title = 'Ubah Data Pasien: ' . $model->norm;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Pasien', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->norm, 'url' => ['view', 'norm' => $model->norm]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="trx-pasien-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
