<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TrxUser $model */

$this->title = 'Update Trx User: ' . $model->id_user;
$this->params['breadcrumbs'][] = ['label' => 'Trx Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_user, 'url' => ['view', 'id_user' => $model->id_user]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trx-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
