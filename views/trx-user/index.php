<?php

use app\models\TrxUser;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\TrxUserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Trx Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Trx User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_user',
            'nama',
            'user_name',
            'password',
            'role.nama_role',
            //'is_active',
            //'waktu_input',
            //'waktu_update',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, TrxUser $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_user' => $model->id_user]);
                 }
            ],
        ],
    ]); ?>


</div>
