<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\TrxUser $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="trx-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?php
    $dataPost=ArrayHelper::map(\app\models\MasterUserrole::find()->asArray()->all(), 'role_id', 'nama_role');
	echo $form->field($model, 'role_id')
        ->dropDownList(
            $dataPost,           
            ['role_id'=>'nama_role']
        );
        ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
