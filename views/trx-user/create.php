<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TrxUser $model */

$this->title = 'Input Data User';
$this->params['breadcrumbs'][] = ['label' => 'Trx Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
