<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\TrxPendaftaranSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="trx-pendaftaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pendaftaran') ?>

    <?= $form->field($model, 'no_registrasi') ?>

    <?= $form->field($model, 'waktu_registrasi') ?>

    <?= $form->field($model, 'norm') ?>

    <?= $form->field($model, 'id_jenis_registrasi') ?>

    <?php // echo $form->field($model, 'id_jenis_layanan') ?>

    <?php // echo $form->field($model, 'id_jenis_pembayaran') ?>

    <?php // echo $form->field($model, 'id_status_registrasi') ?>

    <?php // echo $form->field($model, 'waktu_selesai_pelayanan') ?>

    <?php // echo $form->field($model, 'id_user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
