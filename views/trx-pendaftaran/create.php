<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TrxPendaftaran $model */

$this->title = 'Buat Pendaftaran';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Kunjungan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-pendaftaran-create">
    <?php 
    $request = Yii::$app->request;
    $norm = $request->get('norm');

    
    $noreg1 =  $model->getNoregistrasi();
    $noreg = (float) $noreg1;

    ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'norm' => $norm,
        'noreg' => $noreg
    ]) ?>

</div>
