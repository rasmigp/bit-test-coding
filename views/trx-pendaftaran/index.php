<?php

use app\models\TrxPendaftaran;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\TrxPendaftaranSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Daftar Kunjungan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-pendaftaran-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div>
        <div class="row">
            <div class="col">
                <?= Html::a('Input Pendaftaran', ['trx-pasien/index'], ['class' => 'btn btn-success']) ?>

            </div>
            <div class="col d-flex justify-content-end">

                <a href="../views/trx-pendaftaran/print_laporan.php" class="btn btn-secondary" target="_blank">Print</a>
            </div>
        </div>
    </div>

 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php echo "lorem "; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_pendaftaran',
            'no_registrasi',
            'waktu_registrasi',
            'norm',
            'norm0.nama',
            'norm0.jenis_kelamin',
            'norm0.tanggal_lahir',
            'jenisRegistrasi.nama_registrasi',
            'jenisLayanan.nama_layanan',
            'jenisPembayaran.nama_pembayaran',
            'statusRegistrasi.nama_registrasi',
            'waktu_registrasi',
            'waktu_selesai_pelayanan',
            'user.nama',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, TrxPendaftaran $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_pendaftaran' => $model->id_pendaftaran]);
                 }
            ],
        ],
    ]); ?>


</div>
