<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\TrxPendaftaran $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="trx-pendaftaran-form">
    <?php //echo $norm; ?>

    <?php $form = ActiveForm::begin(); ?>

    <?php //$form->field($model, 'no_registrasi')->textInput() ?>
    <?= $form->field($model, 'no_registrasi')->textInput(['readonly' => true, 'value' => $model->isNewRecord ? $noreg : $model->no_registrasi]) ?>

    <?php //$form->field($model, 'waktu_registrasi')->textInput() ?>

    <?php //$form->field($model, 'norm')->textInput() ?>
    <?= $form->field($model, 'norm')->textInput(['readonly' => true, 'value' => $model->isNewRecord ? $norm : $model->norm]) ?>

    <?php //$form->field($model, 'id_jenis_registrasi')->textInput() ?>
    <?php
        $dataPost=ArrayHelper::map(\app\models\MasterJenisregistrasi::find()->asArray()->all(), 'id_jenis_registrasi', 'nama_registrasi');
        echo $form->field($model, 'id_jenis_registrasi')
            ->dropDownList(
                $dataPost,           
                ['id_jenis_registrasi'=>'nama_registrasi']
            );
    ?>

    <?php
        $dataPost=ArrayHelper::map(\app\models\MasterJenislayanan::find()->asArray()->all(), 'id_jenis_layanan', 'nama_layanan');
        echo $form->field($model, 'id_jenis_layanan')
            ->dropDownList(
                $dataPost,           
                ['id_jenis_layanan'=>'nama_layanan']
            );
    ?>

    <?php
        $dataPost=ArrayHelper::map(\app\models\MasterJenispembayaran::find()->asArray()->all(), 'id_jenis_pembayaran', 'nama_pembayaran');
        echo $form->field($model, 'id_jenis_pembayaran')
            ->dropDownList(
                $dataPost,           
                ['id_jenis_pembayaran'=>'nama_pembayaran']
            );
    ?>

    <?php
        $dataPost=ArrayHelper::map(\app\models\MasterStatusregistrasi::find()->asArray()->all(), 'id_status_registrasi', 'nama_registrasi');
        echo $form->field($model, 'id_status_registrasi')
            ->dropDownList(
                $dataPost,           
                ['id_status_registrasi'=>'nama_registrasi']
            );
    ?>

    <?php // $form->field($model, 'waktu_selesai_pelayanan')->textInput() ?>

    <?php // $form->field($model, 'id_user')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
