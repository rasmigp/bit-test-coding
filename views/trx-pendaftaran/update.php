<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\TrxPendaftaran $model */

$this->title = 'Update Trx Pendaftaran: ' . $model->id_pendaftaran;
$this->params['breadcrumbs'][] = ['label' => 'Trx Pendaftarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pendaftaran, 'url' => ['view', 'id_pendaftaran' => $model->id_pendaftaran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trx-pendaftaran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
