<?php

use app\models\MasterUserrole;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\MasterUserroleSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Master Userroles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-userrole-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Master Userrole', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'role_id',
            'nama_role',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, MasterUserrole $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'role_id' => $model->role_id]);
                 }
            ],
        ],
    ]); ?>


</div>
