<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterUserrole $model */

$this->title = 'Create Master Userrole';
$this->params['breadcrumbs'][] = ['label' => 'Master Userroles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-userrole-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
