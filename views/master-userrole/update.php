<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterUserrole $model */

$this->title = 'Update Master Userrole: ' . $model->role_id;
$this->params['breadcrumbs'][] = ['label' => 'Master Userroles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->role_id, 'url' => ['view', 'role_id' => $model->role_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-userrole-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
