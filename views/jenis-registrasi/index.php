<?php

use app\models\MasterJenisRegistrasi;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\JenisRegistrasiSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Jenis Registrasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-jenis-registrasi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambah Jenis Registrasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_jenis_registrasi',
            'nama_registrasi',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, MasterJenisRegistrasi $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_jenis_registrasi' => $model->id_jenis_registrasi]);
                 }
            ],
        ],
    ]); ?>


</div>
