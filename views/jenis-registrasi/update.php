<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterJenisRegistrasi $model */

$this->title = 'Update Master Jenis Registrasi: ' . $model->id_jenis_registrasi;
$this->params['breadcrumbs'][] = ['label' => 'Master Jenis Registrasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_jenis_registrasi, 'url' => ['view', 'id_jenis_registrasi' => $model->id_jenis_registrasi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-jenis-registrasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
