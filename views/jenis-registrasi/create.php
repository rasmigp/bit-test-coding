<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterJenisRegistrasi $model */

$this->title = 'Buat Jenis Registrasi';
$this->params['breadcrumbs'][] = ['label' => 'Master Jenis Registrasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-jenis-registrasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
