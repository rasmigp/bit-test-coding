<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\MasterJenislayanan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="master-jenislayanan-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php
    $dataPost=ArrayHelper::map(\app\models\MasterJenisregistrasi::find()->asArray()->all(), 'id_jenis_registrasi', 'nama_registrasi');
	echo $form->field($model, 'id_jenis_registrasi')
        ->dropDownList(
            $dataPost,           
            ['id_jenis_registrasi'=>'nama_registrasi']
        );
        ?>

    <?= $form->field($model, 'nama_layanan')->textInput(['maxlength' => true]) ?>

   

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
