<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterJenislayanan $model */

$this->title = 'Update Master Jenislayanan: ' . $model->id_jenis_layanan;
$this->params['breadcrumbs'][] = ['label' => 'Master Jenislayanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_jenis_layanan, 'url' => ['view', 'id_jenis_layanan' => $model->id_jenis_layanan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-jenislayanan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
