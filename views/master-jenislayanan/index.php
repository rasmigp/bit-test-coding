<?php

use app\models\MasterJenislayanan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\MasterJenislayananSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Master Jenislayanans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-jenislayanan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Master Jenislayanan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_jenis_layanan',
            'jenisRegistrasi.nama_registrasi',
            'nama_layanan',
            'is_active',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, MasterJenislayanan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_jenis_layanan' => $model->id_jenis_layanan]);
                 }
            ],
        ],
    ]); ?>


</div>
