<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\MasterJenispembayaran $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="master-jenispembayaran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_pembayaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
