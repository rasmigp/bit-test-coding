<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterJenispembayaran $model */

$this->title = 'Update Master Jenispembayaran: ' . $model->id_jenis_pembayaran;
$this->params['breadcrumbs'][] = ['label' => 'Master Jenispembayarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_jenis_pembayaran, 'url' => ['view', 'id_jenis_pembayaran' => $model->id_jenis_pembayaran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-jenispembayaran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
