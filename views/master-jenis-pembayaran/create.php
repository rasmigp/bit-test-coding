<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterJenispembayaran $model */

$this->title = 'Create Master Jenispembayaran';
$this->params['breadcrumbs'][] = ['label' => 'Master Jenispembayarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-jenispembayaran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
