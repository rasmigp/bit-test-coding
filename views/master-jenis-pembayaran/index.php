<?php

use app\models\MasterJenispembayaran;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\MasterJenispembayaranSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Master Jenispembayarans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-jenispembayaran-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Master Jenispembayaran', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_jenis_pembayaran',
            'nama_pembayaran',
            'is_active',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, MasterJenispembayaran $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_jenis_pembayaran' => $model->id_jenis_pembayaran]);
                 }
            ],
        ],
    ]); ?>


</div>
