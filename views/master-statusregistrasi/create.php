<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterStatusregistrasi $model */

$this->title = 'Create Master Statusregistrasi';
$this->params['breadcrumbs'][] = ['label' => 'Master Statusregistrasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-statusregistrasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
