<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MasterStatusregistrasi $model */

$this->title = 'Update Master Statusregistrasi: ' . $model->id_status_registrasi;
$this->params['breadcrumbs'][] = ['label' => 'Master Statusregistrasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_status_registrasi, 'url' => ['view', 'id_status_registrasi' => $model->id_status_registrasi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-statusregistrasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
