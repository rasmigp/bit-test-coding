<?php

use app\models\MasterStatusregistrasi;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\MasterStatusregistrasiSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Master Statusregistrasis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-statusregistrasi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Master Statusregistrasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_status_registrasi',
            'nama_registrasi',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, MasterStatusregistrasi $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_status_registrasi' => $model->id_status_registrasi]);
                 }
            ],
        ],
    ]); ?>


</div>
