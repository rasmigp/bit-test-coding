<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\MasterStatusregistrasi $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="master-statusregistrasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_registrasi')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
