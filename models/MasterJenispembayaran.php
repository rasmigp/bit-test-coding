<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_jenispembayaran".
 *
 * @property int $id_jenis_pembayaran
 * @property string|null $nama_pembayaran
 * @property int|null $is_active
 *
 * @property TrxPendaftaran[] $trxPendaftarans
 */
class MasterJenispembayaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_jenispembayaran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['nama_pembayaran'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jenis_pembayaran' => 'Id Jenis Pembayaran',
            'nama_pembayaran' => 'Nama Pembayaran',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * Gets query for [[TrxPendaftarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPendaftarans()
    {
        return $this->hasMany(TrxPendaftaran::class, ['id_jenis_pembayaran' => 'id_jenis_pembayaran']);
    }
}
