<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "trx_pasien".
 *
 * @property int $norm
 * @property string|null $nama
 * @property string|null $tempat_lahir
 * @property string|null $tanggal_lahir
 * @property int|null $jenis_kelamin
 * @property int|null $is_active
 * @property string|null $waktu_input
 * @property string|null $waktu_update
 *
 * @property MasterJk $jenisKelamin
 * @property TrxPendaftaran[] $trxPendaftarans
 */
class TrxPasien extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_pasien';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tanggal_lahir', 'waktu_input', 'waktu_update'], 'safe'],
            [['jenis_kelamin', 'is_active'], 'integer'],
            [['nama', 'tempat_lahir'], 'string', 'max' => 64],
            [['jenis_kelamin'], 'exist', 'skipOnError' => true, 'targetClass' => MasterJk::class, 'targetAttribute' => ['jenis_kelamin' => 'id_jk']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'norm' => 'Norm',
            'nama' => 'Nama',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'is_active' => 'Is Active',
            'waktu_input' => 'Waktu Input',
            'waktu_update' => 'Waktu Update',
        ];
    }

    /**
     * Gets query for [[JenisKelamin]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenisKelamin()
    {
        return $this->hasOne(MasterJk::class, ['id_jk' => 'jenis_kelamin']);
    }

    /**
     * Gets query for [[TrxPendaftarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPendaftarans()
    {
        return $this->hasMany(TrxPendaftaran::class, ['norm' => 'norm']);
    }

    public function behaviors()
    {
        return [
            // Other behaviors
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_input',
                'updatedAtAttribute' => 'waktu_update',
                'value' => new Expression('NOW()'),
            ],
        ];   
    }
}
