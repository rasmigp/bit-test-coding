<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_jenislayanan".
 *
 * @property int $id_jenis_layanan
 * @property int|null $id_jenis_registrasi
 * @property string|null $nama_layanan
 * @property int|null $is_active
 *
 * @property MasterJenisregistrasi $jenisRegistrasi
 * @property TrxPendaftaran[] $trxPendaftarans
 */
class MasterJenislayanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_jenislayanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jenis_registrasi', 'is_active'], 'integer'],
            [['nama_layanan'], 'string', 'max' => 32],
            [['id_jenis_registrasi'], 'exist', 'skipOnError' => true, 'targetClass' => MasterJenisregistrasi::class, 'targetAttribute' => ['id_jenis_registrasi' => 'id_jenis_registrasi']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jenis_layanan' => 'Id Jenis Layanan',
            'id_jenis_registrasi' => 'Id Jenis Registrasi',
            'nama_layanan' => 'Nama Layanan',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * Gets query for [[JenisRegistrasi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenisRegistrasi()
    {
        return $this->hasOne(MasterJenisregistrasi::class, ['id_jenis_registrasi' => 'id_jenis_registrasi']);
    }

    /**
     * Gets query for [[TrxPendaftarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPendaftarans()
    {
        return $this->hasMany(TrxPendaftaran::class, ['id_jenis_layanan' => 'id_jenis_layanan']);
    }
}
