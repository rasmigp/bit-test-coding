<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_userrole".
 *
 * @property int $role_id
 * @property string|null $nama_role
 *
 * @property TrxUser[] $trxUsers
 */
class MasterUserrole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_userrole';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_role'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'nama_role' => 'Nama Role',
        ];
    }

    /**
     * Gets query for [[TrxUsers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxUsers()
    {
        return $this->hasMany(TrxUser::class, ['role_id' => 'role_id']);
    }
}
