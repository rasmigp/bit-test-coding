<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterJenislayanan;

/**
 * MasterJenislayananSearch represents the model behind the search form of `app\models\MasterJenislayanan`.
 */
class MasterJenislayananSearch extends MasterJenislayanan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jenis_layanan', 'id_jenis_registrasi', 'is_active'], 'integer'],
            [['nama_layanan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterJenislayanan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jenis_layanan' => $this->id_jenis_layanan,
            'id_jenis_registrasi' => $this->id_jenis_registrasi,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'nama_layanan', $this->nama_layanan]);

        return $dataProvider;
    }
}
