<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrxPasien;

/**
 * TrxPasienSearch represents the model behind the search form of `app\models\TrxPasien`.
 */
class TrxPasienSearch extends TrxPasien
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['norm', 'jenis_kelamin', 'is_active'], 'integer'],
            [['nama', 'tempat_lahir', 'tanggal_lahir', 'waktu_input', 'waktu_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxPasien::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'norm' => $this->norm,
            'tanggal_lahir' => $this->tanggal_lahir,
            'jenis_kelamin' => $this->jenis_kelamin,
            'is_active' => $this->is_active,
            'waktu_input' => $this->waktu_input,
            'waktu_update' => $this->waktu_update,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir]);

        return $dataProvider;
    }
}
