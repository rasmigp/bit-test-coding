<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_jk".
 *
 * @property int $id_jk
 * @property string|null $nama_jk
 *
 * @property TrxPasien[] $trxPasiens
 */
class MasterJk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_jk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_jk'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jk' => 'Id Jk',
            'nama_jk' => 'Nama Jk',
        ];
    }

    /**
     * Gets query for [[TrxPasiens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPasiens()
    {
        return $this->hasMany(TrxPasien::class, ['jenis_kelamin' => 'id_jk']);
    }
}
