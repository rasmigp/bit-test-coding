<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrxPendaftaran;

/**
 * TrxPendaftaranSearch represents the model behind the search form of `app\models\TrxPendaftaran`.
 */
class TrxPendaftaranSearch extends TrxPendaftaran
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pendaftaran', 'no_registrasi', 'norm', 'id_jenis_registrasi', 'id_jenis_layanan', 'id_jenis_pembayaran', 'id_status_registrasi', 'id_user'], 'integer'],
            [['waktu_registrasi', 'waktu_selesai_pelayanan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxPendaftaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pendaftaran' => $this->id_pendaftaran,
            'no_registrasi' => $this->no_registrasi,
            'waktu_registrasi' => $this->waktu_registrasi,
            'norm' => $this->norm,
            'id_jenis_registrasi' => $this->id_jenis_registrasi,
            'id_jenis_layanan' => $this->id_jenis_layanan,
            'id_jenis_pembayaran' => $this->id_jenis_pembayaran,
            'id_status_registrasi' => $this->id_status_registrasi,
            'waktu_selesai_pelayanan' => $this->waktu_selesai_pelayanan,
            'id_user' => $this->id_user,
        ]);

        return $dataProvider;
    }
}
