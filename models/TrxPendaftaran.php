<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "trx_pendaftaran".
 *
 * @property int $id_pendaftaran
 * @property int|null $no_registrasi
 * @property string|null $waktu_registrasi
 * @property int|null $norm
 * @property int|null $id_jenis_registrasi
 * @property int|null $id_jenis_layanan
 * @property int|null $id_jenis_pembayaran
 * @property int|null $id_status_registrasi
 * @property string|null $waktu_selesai_pelayanan
 * @property int|null $id_user
 *
 * @property MasterJenislayanan $jenisLayanan
 * @property MasterJenispembayaran $jenisPembayaran
 * @property MasterJenisregistrasi $jenisRegistrasi
 * @property TrxPasien $norm0
 * @property MasterStatusregistrasi $statusRegistrasi
 * @property MasterStatusregistrasi $statusRegistrasi0
 * @property TrxUser $user
 */
class TrxPendaftaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_pendaftaran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_registrasi', 'norm', 'id_jenis_registrasi', 'id_jenis_layanan', 'id_jenis_pembayaran', 'id_status_registrasi', 'id_user'], 'integer'],
            [['waktu_registrasi', 'waktu_selesai_pelayanan'], 'safe'],
            [['norm'], 'exist', 'skipOnError' => true, 'targetClass' => TrxPasien::class, 'targetAttribute' => ['norm' => 'norm']],
            [['id_jenis_registrasi'], 'exist', 'skipOnError' => true, 'targetClass' => MasterJenisregistrasi::class, 'targetAttribute' => ['id_jenis_registrasi' => 'id_jenis_registrasi']],
            [['id_jenis_layanan'], 'exist', 'skipOnError' => true, 'targetClass' => MasterJenislayanan::class, 'targetAttribute' => ['id_jenis_layanan' => 'id_jenis_layanan']],
            [['id_jenis_pembayaran'], 'exist', 'skipOnError' => true, 'targetClass' => MasterJenispembayaran::class, 'targetAttribute' => ['id_jenis_pembayaran' => 'id_jenis_pembayaran']],
            [['id_status_registrasi'], 'exist', 'skipOnError' => true, 'targetClass' => MasterStatusregistrasi::class, 'targetAttribute' => ['id_status_registrasi' => 'id_status_registrasi']],
            [['id_status_registrasi'], 'exist', 'skipOnError' => true, 'targetClass' => MasterStatusregistrasi::class, 'targetAttribute' => ['id_status_registrasi' => 'id_status_registrasi']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => TrxUser::class, 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pendaftaran' => 'Id Pendaftaran',
            'no_registrasi' => 'No Registrasi',
            'waktu_registrasi' => 'Waktu Registrasi',
            'norm' => 'Norm',
            'id_jenis_registrasi' => 'Id Jenis Registrasi',
            'id_jenis_layanan' => 'Id Jenis Layanan',
            'id_jenis_pembayaran' => 'Id Jenis Pembayaran',
            'id_status_registrasi' => 'Id Status Registrasi',
            'waktu_selesai_pelayanan' => 'Waktu Selesai Pelayanan',
            'id_user' => 'Id User',
        ];
    }

    /**
     * Gets query for [[JenisLayanan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenisLayanan()
    {
        return $this->hasOne(MasterJenislayanan::class, ['id_jenis_layanan' => 'id_jenis_layanan']);
    }

    /**
     * Gets query for [[JenisPembayaran]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPembayaran()
    {
        return $this->hasOne(MasterJenispembayaran::class, ['id_jenis_pembayaran' => 'id_jenis_pembayaran']);
    }

    /**
     * Gets query for [[JenisRegistrasi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenisRegistrasi()
    {
        return $this->hasOne(MasterJenisregistrasi::class, ['id_jenis_registrasi' => 'id_jenis_registrasi']);
    }

    /**
     * Gets query for [[Norm0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNorm0()
    {
        return $this->hasOne(TrxPasien::class, ['norm' => 'norm']);
    }

    /**
     * Gets query for [[StatusRegistrasi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatusRegistrasi()
    {
        return $this->hasOne(MasterStatusregistrasi::class, ['id_status_registrasi' => 'id_status_registrasi']);
    }

    // /**
    //  * Gets query for [[StatusRegistrasi0]].
    //  *
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getStatusRegistrasi0()
    // {
    //     return $this->hasOne(MasterStatusregistrasi::class, ['id_status_registrasi' => 'id_status_registrasi']);
    // }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(TrxUser::class, ['id_user' => 'id_user']);
    }

    public function behaviors()
    {
        return [
            // Other behaviors
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_registrasi',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];   
    }

    public function getNoregistrasi()
    {
        // $rowsq = (new \yii\db\Query())->select(['*'])->from('trx_user');    
        // $jmlh = count($rowsq);
        // return $query->from('user');;

        //Creates new record as per request
        //Connect to database
        $hostname = "localhost";    //example = localhost or 192.168.0.0
        $username = "root";   //example = root
        $password = ""; 
        $dbname = "instansikesehatan";
        // Create connection
        $koneksi = mysqli_connect($hostname, $username, $password, $dbname);

        date_default_timezone_set('Asia/Jakarta');
        $tglsekarang = date('Y-m-d');

        $tabel=mysqli_query($koneksi, "SELECT * FROM trx_pendaftaran WHERE DATE(waktu_registrasi) = '$tglsekarang'");
        $jmlhpengujung=mysqli_num_rows($tabel);
        $no = (int) $jmlhpengujung + 1;
        $jmlhdftr = sprintf("%04d", $no);
        $noregistrasi = date('ymd').$jmlhdftr;
        return $noregistrasi;
    }

}
