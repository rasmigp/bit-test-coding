<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_statusregistrasi".
 *
 * @property int $id_status_registrasi
 * @property string|null $nama_registrasi
 *
 * @property TrxPendaftaran[] $trxPendaftarans
 * @property TrxPendaftaran[] $trxPendaftarans0
 */
class MasterStatusregistrasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_statusregistrasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_registrasi'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_status_registrasi' => 'Id Status Registrasi',
            'nama_registrasi' => 'Nama Registrasi',
        ];
    }

    /**
     * Gets query for [[TrxPendaftarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPendaftarans()
    {
        return $this->hasMany(TrxPendaftaran::class, ['id_status_registrasi' => 'id_status_registrasi']);
    }

    /**
     * Gets query for [[TrxPendaftarans0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPendaftarans0()
    {
        return $this->hasMany(TrxPendaftaran::class, ['id_status_registrasi' => 'id_status_registrasi']);
    }
}
