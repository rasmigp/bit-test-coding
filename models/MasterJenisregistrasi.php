<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_jenisregistrasi".
 *
 * @property int $id_jenis_registrasi
 * @property string|null $nama_registrasi
 *
 * @property TrxPendaftaran[] $trxPendaftarans
 */
class MasterJenisregistrasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_jenisregistrasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_registrasi'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jenis_registrasi' => 'Id Jenis Registrasi',
            'nama_registrasi' => 'Nama Registrasi',
        ];
    }

    /**
     * Gets query for [[TrxPendaftarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPendaftarans()
    {
        return $this->hasMany(TrxPendaftaran::class, ['id_jenis_registrasi' => 'id_jenis_registrasi']);
    }
}
