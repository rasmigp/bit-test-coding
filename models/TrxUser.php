<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "trx_user".
 *
 * @property int $id_user
 * @property string $nama
 * @property string|null $user_name
 * @property string|null $password
 * @property int|null $role_id
 * @property int|null $is_active
 * @property string|null $waktu_input
 * @property string|null $waktu_update
 *
 * @property MasterUserrole $role
 * @property TrxPendaftaran[] $trxPendaftarans
 */
class TrxUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['role_id', 'is_active'], 'integer'],
            [['waktu_input', 'waktu_update'], 'safe'],
            [['nama', 'user_name'], 'string', 'max' => 64],
            [['password'], 'string', 'max' => 32],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUserrole::class, 'targetAttribute' => ['role_id' => 'role_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'nama' => 'Nama',
            'user_name' => 'User Name',
            'password' => 'Password',
            'role_id' => 'Role ID',
            'is_active' => 'Is Active',
            'waktu_input' => 'Waktu Input',
            'waktu_update' => 'Waktu Update',
        ];
    }

    /**
     * Gets query for [[Role]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(MasterUserrole::class, ['role_id' => 'role_id']);
    }

    /**
     * Gets query for [[TrxPendaftarans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrxPendaftarans()
    {
        return $this->hasMany(TrxPendaftaran::class, ['id_user' => 'id_user']);
    }

    public function behaviors()
    {
        return [
            // Other behaviors
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'waktu_input',
                'updatedAtAttribute' => 'waktu_update',
                'value' => new Expression('NOW()'),
            ],
        ];   
    }
}
