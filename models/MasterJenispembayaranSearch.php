<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterJenispembayaran;

/**
 * MasterJenispembayaranSearch represents the model behind the search form of `app\models\MasterJenispembayaran`.
 */
class MasterJenispembayaranSearch extends MasterJenispembayaran
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jenis_pembayaran', 'is_active'], 'integer'],
            [['nama_pembayaran'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterJenispembayaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jenis_pembayaran' => $this->id_jenis_pembayaran,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'nama_pembayaran', $this->nama_pembayaran]);

        return $dataProvider;
    }
}
